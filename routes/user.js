const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");
// routes folder is where all http method and end points are located

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// [ACTIVITY] S38 - Code Along
/* 
router.post("/details", (req, res) => {
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
}) */

// Yung arrow function po na iyun is also referred as request and response cycle. Vi-neverify muna yung token bago payagan makapasok sa req-res-cycle para masiguro na di na-breach ang security ng server. --- Trainer Rupert 11/2

router.post("/details", auth.verify, (req, res) => {
	// We can get the token by accessing req.headers.authorization
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

// S41 d1 router - Code Along activity
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;